#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pharse.py

def open_file(file, mode='r'):# open text file
	x = open(file, mode).readlines()
	x = rws(x)
	return x

def syn_replace(inlist, synlist):# takes a list and replaces words with synms from another list
	temp = inlist[:]
	for syn in synlist:
		if syn in temp:
			syncount = temp.count(syn)
			for count in range(syncount):
				x = temp.index(syn)
				temp[x] = synlist[0]
	return temp

def syn_string_replace(instring, synlist):# takes a string and replaces words with syms from a synm list
	temp = " " + instring[:] + " "
	for syn in synlist:
		if syn in temp:
			syncount = temp.count(syn)
			for count in range(syncount):
				x = temp.find(syn)
				if x != -1:
					temp = temp.replace(syn, synlist[0])
	temp = temp[1:len(temp)]
	return temp

def synreader(inlist):# opens a list of lines and converts eachline into a "synline" then appends it to a synlist
	output = []
	count = 0
	for line in inlist:
		currentline = inlist[count]
		count += 1
		index = 0
		line_list = []
		word = ''
		lastcomma = 0
		for char in currentline:
			index += 1
			if char==',':
				word = currentline[lastcomma:(index - 1)]
				line_list.append(word)
				lastcomma = index
		output.append(line_list)
	return output

def rws(inlist, more=0):#remove \n and \r char from the start and end of each item on a list set more to remove more chars
	templist = []
	for line in inlist:
		end = len(line) - 1
		templine = line
		templine = templine[0:] + (templine[:0 + more].replace("\n", "")).replace("\r", "")
		templine = templine[:end] + templine[end - more:].replace("\n", "").replace("\r", "")
		templist.append(templine)
	return templist

def ruleread(list):# take rules list and return a nested list split into: in, out, command type, and command input lists
	xmlfile = ET.parse('config.xml')
	xmldata = xmlfile.getroot()
