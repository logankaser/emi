@echo off
set "extension=.py"
:1
cls
echo To run a python file enter the filename without the file extension
set /p filename="filename:"
set "fullname=%filename%%extension%"
python %fullname%
set /p exit="Exit? (y/n):"
if "%exit%"=="n" (
goto 1
) else (
exit
)