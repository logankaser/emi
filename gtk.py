#!/usr/bin/python
from gi.repository import Gtk, GObject, Gdk, Pango

class Emi_Window(Gtk.Window):

	def __init__(self):
		Gtk.Window.__init__(self, title="Emi")
		self.set_size_request(200, 100)

		self.timeout_id = None

		vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=4)
		self.add(vbox)

		output = Gtk.Label.new()
		output.set_selectable(True)
		output.set_line_wrap(True)
		output.override_font(Pango.FontDescription("InputSans 12"))
		vbox.pack_start(output, False, False, 4)

		def enter(self):
			text = input_box.get_text()
			text = str(text)
			output.set_text(text)
			print(text)
			input_box.set_text("")

		input_box = Gtk.Entry()
		input_box.props.has_frame=False
		input_box.set_placeholder_text(">")
		input_box.set_input_hints(Gtk.InputHints.LOWERCASE)
		input_box.set_input_hints(Gtk.InputHints.SPELLCHECK)
		input_box.override_font(Pango.FontDescription("InputSans 12"))
		enter = input_box.connect("activate", enter)
		vbox.pack_end(input_box, False, False, 0)

win = Emi_Window()
win.connect("delete-event", Gtk.main_quit)
WIN_BACKGROUND = Gdk.RGBA(red=1.0, green=1.0, blue=1.0, alpha=1.0)
win.override_color(Gtk.StateType.NORMAL, WIN_BACKGROUND)
win.show_all()
Gtk.main()